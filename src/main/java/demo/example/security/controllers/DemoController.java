package demo.example.security.controllers;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;

@Controller
public class DemoController {

    @GetMapping("/leaders")
    public String showLeaders() {
        return "leaders";
    }
}

